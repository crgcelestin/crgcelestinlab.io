export default [
    {
        year: 'Today',
        title: 'Software Developer - Hack Reactor Graduate',
        duration: '7 months',
        details:
            ' Participated in Full Stack Architecture Development involving turning Software Requirements into a running application with Python and Django, Vanilla and JS. Wrote HTML and CSS to build working and interactive Web apps. Used a relational PostgreSQL database to store and retrieve data. Used a number of protocols and formats to interact w/ data in messaging middleware. Applied the latest design and technological principles to create Microservices. Created stand-alone web app frontends w/ functional react, hooks, redux. Built real-time apps that update their UIs w/ WebSockets and FastAPI. Used various persistent data stores for different data types with MongoDB, Apache Kafka. Planned, created, and monitored CI and Pipeline Delivery with GitLab Pipelines',
    },
    {
        year: '2023',
        title: 'AWS AI Programming and Machine Learning Fellow',
        duration: '5 months',
        details:
            '140 hours of AI Python Programming Curriculum culminating in two projects: (1) Implementing an algorithmic framework to ID Dog Breeds using a pre trained image classifier (vgg16, densenet, arch), (2) Creating my own image classifier that was able to identify various species of flowers from user provided images - involved training neural networks and using pytorch to design deep learning models'
    },
    {
        year: '2022',
        title: "Graduated From Binghamton University - Bachelor's Degree in Chemistry (Engineering minor)",
        duration: '3 years',
        details:
            'Activities and societies: Treasurer Transfer Student Association, SES (Smart Energy Scholars) Member, Work Study, McNair Graduate Program, NSBE (National Society for Black Engineers)'
    },
    {
        year: '2022',
        title: 'Energy Analyst at COI Energy',
        duration: '1 year',
        details:
            'Researched critical facets of the energy industry, regulations and technology advancements that can affect the business prospects of COI and report on these issues. Participated in the Quality Analysis Process for their web, mobile platforms for their Demand Response for Business, Utility, and Admin Perspective. Monitored, interpreted, inputted, and analyzed daily operations data to identify trends and process improvement opportunities. Developed project timelines and manage critical milestones to ensure on-time and under-budget delivery. Worked with Energy Data and Demand Response modeling using Jupyter Notebook'
    },
    {
        year: '2021',
        title: 'DS4A Fellow',
        duration: '5 months',
        details:
            'Completed a successful mentorship project with partnering bank Creditas (Brazilian Financial Firm) enabling a UI providing customer analytics'
    },
    {
        year: '2019',
        title: 'JP Morgan Chase: Intern for Industry Initiatives (Loan Trading)',
        duration: '3 months',
        details:
            'Supported various projects/process initiatives aimed at reducing settlement times, increasing liquidity and reducing risk in the loan market. Responsible for aiding technology releases. Focused on data analysis and quantifiable metrics. Worked with business partners across WLS to track key project milestones'
    },
]
