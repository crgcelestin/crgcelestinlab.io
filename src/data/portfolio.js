export default [
    {
        title: 'CLOSET IO',
        imgUrl: './assets/fitster.png',
        stack: ['FastAPI', 'ReactJS', 'PostgreSQl', 'Docker', 'CapRover'],
        link: 'https://gitlab.com/crgcelestin/closetio',
    },
    {
        title: 'Car Shop',
        imgUrl: './assets/carshop.png',
        stack: ['ReactJS', 'Python', 'PostgreSQL', 'Django', 'Docker'],
        link: 'https://gitlab.com/crgcelestin/car-shop',
    },
    {
        title: 'Image Classifier',
        imgUrl: './assets/imageclass.png',
        stack: ['Python', 'Numpy (PyTorch)', 'HTML'],
        link: 'https://gitlab.com/crgcelestin/image-classifer',
    },
]
