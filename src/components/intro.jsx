import '/src/styles1.css';
import React from "react";
import { Icon } from '@iconify/react';

function Intro() {
    return (
        <>
            <div className="flex items-center
        justify-center flex-col text-center pt-20
        pb-6">
                <img
                    className="profilePhoto"
                    src={`./assets/profile.jpeg`}
                />
                <h1 className="text-4xl md:text-7xl dark:text-white mb-1
            md:mb-3 font-bold
            "> Craig Celestin </h1>
                <p className="text-base
            md:text-l mb-3 font-medium"> Full-Stack Software Engineer </p>
                <p className="text-sm
            max-w-xl
            ">
                    <div className='font-bold'>
                        Hello!
                    </div>


                    <div>I am an interdisciplinary software engineer with experience in Full Stack agile development and AI Programming. I am curious about software's continued expanse and innovation.
                        My main motivations in software development are to create products that are maintainable, scale quickly, and are focused on ease of use.
                    </div>
                    <div>
                        &nbsp;
                    </div>
                    <p className='font-bold'>
                        | About Me |
                    </p>
                    <div>
                        Since a youth, I have always been around computers, technology, and STEM. My earliest memories of using the personal computer ranged from disjointed narratives written in wordArt to countless hours on Club Penguin.
                    </div>
                    <div>
                        &nbsp;
                    </div>
                    <div>
                        While I was at University, I had the ability to travel to engineering conferences and study abroad implementing renewable systems for a village in Nepal. Over the course of the past year, I have pursued learning opportunities in both my recent curriculum and more recently programs in both Software and Artificial Intelligence. Being able to attend Hack Reactor on scholarship, has opened me to to the world that is out there.
                    </div>
                    <div>
                        &nbsp;
                    </div>
                    <div>
                        At Hack Reactor, I was able pursue learning opportunities in various technologies including React, FastAPI, Django, PostgreSQL, and MongoDB. I am excited about applying all of these concepts and tech acumen to any potential software opportunities!

                        <div>
                            I am looking to connect and am applying to open positions!
                        </div>
                    </div>
                    <div>
                        &nbsp;
                    </div>
                    <div className=" text-xs font-bold
            ">

                        Front End - JavaScript | React | HTML5 | CSS | Bootstrap | DOM Manipulation
                        <div>
                            Back End - Python 3 | Django 4 | MongoDB | PostgresSQL | REST API | FastAPI | RabbitMQ
                        </div>
                        Testing/Deployment - Test Driven Development | Object Oriented Design | Caprover | Docker
                        <div>
                            Development - Gitlab | Dev Tools | VS Code | Insomnia | FastAPI Docs | Agile workflow | 3rd Party APIs
                        </div>
                        <div>
                            Artifical Intelligence - PyTorch | Numpy
                        </div>

                    </div>
                </p>
                <br />
                <div>
                    <a
                        href="https://gitlab.com/crgcelestin" target="_new" rel="noopener noreferrer">
                        <Icon
                            className="contactIcon
                            dark:contactIcon2"
                            icon="ion:logo-gitlab"
                            width="30"
                            height="30"
                        />
                    </a>
                    <a href="http://www.linkedin.com/in/craig-celestin" target="_new" rel="noopener noreferrer">
                        <Icon
                            className="contactIcon dark:contactIcon2"
                            icon="ion:logo-linkedin" width="30" height="30"
                        />
                    </a>
                </div>
            </div>
        </>
    )
}

export default Intro;
